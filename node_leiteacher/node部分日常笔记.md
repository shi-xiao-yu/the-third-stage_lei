## es6

1. let关键字:

### http协议

协议是计算机之间通信要遵循的规则

http(hypertext transport protocol)  协议也叫超文本传输协议,是一种基于TCP/IP的应用层通信协议,规定了浏览器和万维网服务器之间相互通信的规则

客户端和服务器端传输的内容我们称为报文

url:统一资源定位器

uri:统一资源定位符

url组成:协议://域名:端口号/路径?查询字符串#哈希值

ip地址是网络计算机的唯一标识符 

### 分布式应用程序

分布式应用程序是指：[应用程序](https://baike.baidu.com/item/应用程序/5985445)分布在不同[计算机](https://baike.baidu.com/item/计算机/140338)上，通过网络来共同完成一项任务。通常为[服务器](https://baike.baidu.com/item/服务器/100571)/[客户端](https://baike.baidu.com/item/客户端)模式。

### 中间件

介于操作系统和应用程序之间的产品，分布式应用软件借助这种软件在不同的技术之间共享资源。

###  IO 密集型和 CPU 密集型

1、IO 密集型：

数据库仅提供建立的查询插入等操作，复杂的业务逻辑依赖与程序的实现，需要程序与数据库的频繁交互

2、CPU 密集型：

一些复杂的逻辑计算可以在数据库中进行处理，可以依赖与数据库端的存储过程，触发器等功能，减少了程序代码与数据库的交互，减轻访问数据

库带来的 IO 压力，对于装备有高速磁盘阵列的服务器来说，可以实现 CPU 密集型

对于目前，高并发，数据量大的动态网站来说，数据库应该为 IO 密集型。

### buffer

\1. 一个类似于数组的对象.存储的是二进制

\2. 操作buffer效率高,因为相当于直接操作内存

\3. 创建buffer对象的时候,长度就固定了,不可变

### proxy代理

在项目根目录创建vue.config.js

module.exports = {

 devServer: {

  host: 'localhost',

  port: 8080,

  proxy: {

   '/get': {

​    target: 'http://localhost:3000',// 要跨域的域名

​    changeOrigin: true, // 是否开启跨域

   },

   '/api': {

​    target: 'http://localhost:3000',// 要跨域的域名

​    changeOrigin: true, // 是否开启跨域

​    pathRewrite: {  //重写路径

​     '^/api': '/api'  // 这种接口配置出来   http://XX.XX.XX.XX:8083/api/login

​     //'^/api': '/' 这种接口配置出来   http://XX.XX.XX.XX:8083/login

   }

  }

 }

}



### nginx

Nginx是一款轻量级的Web 服务器/反向代理服务器及电子邮件（IMAP/POP3）代理服务器，在BSD-like 协议下发行。其特点是占有内存少，并发能力强

### 工程化,组件化,模块化

工程化:将前端项目当成一项系统工程进行分析、组织和构建从而达到项目结构清晰、分工明确、团队配合默契、开发效率提高的目的.工程化是一种思想而不是某种技术

模块化:要写一个实现A功能的JS代码，这个功能在项目其他位置也需要用到，那么我们就可以把这个功能看成一个模块采用一定的方式进行模块化编写，既能实现复用还可以分而治之

组件化:组件化将页面视为一个容器，页面上各个独立部分例如：头部、导航、焦点图、侧边栏、底部等视为独立组件，不同的页面根据内容的需要

### localStorage

Chrome浏览器中localStorage最大5120kb，即5M

### cookie

1. 存值不大(最大可以存4kb)
2. 每个域名下最多存储50条数据(不同浏览器会有不同)
3. 可以自己设置过期时间
4. 具有不可跨域性

### vue3  setup

setup执行时机:

在beforeCreate之前执行一次,吃屎组件对象还没有创建出来 

this是undefined  不能通过this访问  data/computed/methods/props

其实所有的composition API  相关的回调函数也都不可以

setip的返回值

一包 返回yield对象:为模板提供数据,也就是模板中可以直接使用此对象中的所有属性/方法

返回对象中的属性会与data函数返回对象的属性合并称为组件对象的属性

返回对象中的方法会与methods中的方法合并成为组件对象的方法

如果有聪明 setup优先

注意:

 一般不用混合使用:methods中可以访问setup提供的属性和方法,但在setup方法中不能访问data和methods

setup不能是一个async函数:因为返回值不在return的一个对象 ,而是promise, 模板看不到return对象中的属性数据



setup 的参数

- setup(props, context) / setup(props, {attrs, slots, emit})
- props: 包含 props 配置声明且传入了的所有属性的对象
- attrs: 包含没有在 props 配置中声明的属性的对象, 相当于 this.$attrs
- slots: 包含所有传入的插槽内容的对象, 相当于 this.$slots
- emit: 用来分发自定义事件的函数, 相当于 this.$emit

### 性能优化:

拼接字符串,因为字符串不可变,for循环的时候每一次for循环就拼接一次字符串,拼接几次就生成几次,但我们只需要最后一次,所以不要用字符串拼接,定义一个数组,push到数组中,再join转成字符串

### git命令

git init   ---->   git add .	---->	git commit -m 	---->	git log(提交后的日志 q退出) 

回滚:

git checkout -- 文件名(有修改或者新的文件-->已控制文件)  git reset --soft 版本号(版本库回退到暂存区)

git reset HEAD 文件名(暂存区--->工作区)   git reset --mix 版本号(版本库回退到新修改的状态)

git reset --hard 版本号(版本库回退到初始状态)

分支:

创建分支:git branch 分支名   查看分支:git branch  切换分支:git checkout 分支

现在住分支上master有bug  行功能还在开发阶段 切换修复向上bug不影响现有开发内容?

git  checkout master(切换到主分支)---->git branch fixbug(创建yield修改bug的分支)---->git checkout fixbug---->git branch -d fixbug :修改完bug后还需要和主分支合并(1 git checkout master     2    git merge fixbug     git branch -d fixbug)

远端:

1 全局进行限制:git config --global user.name 'xxxx' ---> git config --global user.email 'xxx.@qq.com' 

2 推送到远端的仓库: git remote add origin http://xxxx    给远程仓库起名origin,用origin代替后面的链接

git push -u origin master

3 克隆仓库代码: git clone http://xxxx

4  拉取公司代码:  git checkout dev  ---> git merge msater--->git add .   git commit -m 'xxx'  git push origin dev

5 公司继续开发: git checkout dev ---> git pull origin dev  ---> git add .   git commit -m 'xxx'  git push origin dev

6 开发完毕上线: 

1 将dev分支合并到master进行上线git checkout msater --> git merge dev  ---> git push origin master  

2 把dev也推送到远程  git checkout dev  --->git merget master --->git push origin dev

### gulp

1   gulp是与webpack功能类似的前端项目构建工具 也是基于nodejs的自动任务运行器  
2   能自动化的完成文件的合并压缩 检查  监听文件的变化 浏览器自动刷新 测试等任务  
3   gulp更高效异步多任务   更易于使用 插件高质量



相关的插件

gulp-concat 合并js/css文件  gulp-cssmin 压缩css   gulp-uglify 压缩js文件	gulp-rename  文件重命名  gulp-livereload  实时自动贬义词刷新



重要的api

gulp.src(filepath/patharr)   指向指定路径的所有文件 返回文件流对象  用于读取文件

gulp.dest(dirPath/pathArr) 指向指定的所有文件夹  用于向文件夹中输出文件

gulp.task(name,[deps],fn) 定义一个任务

gulp.watch()监视文件的变化



### webpack

webpack是一个模块打包器(bundler)

在webpack看来,前端的所有资源文件都会作为模块处理

他将根据模块的依赖关系进行静态分析,生成对应的静态资源





五个核心概念:

entry:入口起点,指示webpack应该使用哪个模块,来作为构建其内部依赖图的开始

output: 告诉webpack在哪里输出它所创建的bundles,以及如何命名这些文件,默认值为./dist

loader:去处理那些非js/json文件

plugins:插件可以执行的范围更大,执行loader执行不了的事

mode:模式,有生产模式production和开发模式development



理解loader:

webpack只能加载js/json模块,如果要加载其他类型的文件(模块)就需要使用对应的loader进行转换/加载

loader本身也是运行在node.js环境中的js模块

它本身也是一个函数,接收源文件作为参数,返回转换的结果

loader一般以xxx-loader的方式命名,xxx代表了这个loader要做的转换功能,比如json-loader



理解plugins:

插件可以完成一些loader不能完成的功能

插件的使用一般是在webpack的配置信息plugins选项中指定的



配置文件(默认)

webpack.config.js:是一个node模块,返回一个json格式的配置信息对象