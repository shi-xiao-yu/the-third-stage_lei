const { resolve } = require("path");
//插件都需要手动引入 loader会自动加载  打包html文件
const HtmlWebpackPlugin = require("html-webpack-plugin");
module.exports = {
  entry: "./src/js/app.js",
  output: {
    filename: "js/app.js",
    path: resolve(__dirname, "../dist"),
  },
  //定义loader
  module: {
    rules: [
      //打包less资源
      {
        test: /\.less$/, // 检查文件是否以.less结尾（检查是否是less文件）
        use: [
          // 数组中loader执行是从下到上，从右到左顺序执行
          "style-loader", // 创建style标签，添加上js中的css代码
          "css-loader", // 将css以commonjs方式整合到js文件中
          "less-loader", // 将less文件解析成css文件
        ],
      },
      //js语法检查 eslint
      {
        test: /\.js$/, //只检测js文件
        exclude: /node_modules/, //排除node_modules文件夹
        enforce: "pre", //提前加载使用 1. pre 优先处理 2. normal 正常处理（默认)3. inline 其次处理 4. post 最后处理
        use: {
          //使用eslint-loader解析
          loader: "eslint-loader",
        },
      },
      //js语法转换  比如箭头函数转换成普通函数
      /* {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                  loader: "babel-loader",
                  options: {
                    presets: ['@babel/preset-env']
                  }
              }
              }, */
      //js语法转换配合按需加载promise的第二种方法使用core.js按需引入   原文件16.4kb  全部引入460kb  按需引入
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: "babel-loader",
          // 注意: presets是一个数组,里面又套了一个数组
          options: {
            presets: [
              [
                "@babel/preset-env",
                {
                  useBuiltIns: "usage", // 按需引入(需要使用polyfill)
                  corejs: { version: 3 }, // 解决warning警告
                  targets: {
                    // 指定兼容性处理哪些浏览器
                    chrome: "58",
                    ie: "9",
                  },
                },
              ],
            ],
            cacheDirectory: true, // 开启babel缓存
          },
        },
      },
      //  打包样式文件中的图片资源
      {
        test: /\.(png|jpg|gif)$/,
        use: {
          loader: "url-loader",
          options: {
            limit: 8192, // 8kb --> 8kb以下的图片会base64处理
            outputPath: "imgs", // 决定文件本地输出路径
            publicPath: "./imgs", // 决定图片的url路径
            name: "[hash:8].[ext]", // 修改文件名称 [hash:8] hash值取8位  [ext] 文件扩展名
          },
        },
      },
      //打包html中的图片资源  url-loader处理imgs文件夹的图片资源 没办法处理html中引入的html资源  注意版本问题 1.3.2
      {
        test: /\.(html)$/,
        use: {
          loader: "html-loader",
        },
      },
      //打包其他资源这里举例为图标字体  1需要添加字体文件  2 需要修改样式css文件中的样式  3 修改html  添加字体  4  配置loader
      {
        test: /\.(eot|svg|woff|woff2|ttf|mp3|mp4|avi)$/, // 处理其他资源
        loader: "file-loader",
        options: {
          outputPath: "media",
          name: "[hash:8].[ext]",
        },
      },
    ],
  },
  //配置插件plugins
  plugins: [
    //打包html文件 注意版本  不要在html中引入任何的css和js文件
    new HtmlWebpackPlugin({
      template: "./src/index.html", // 以当前文件为模板创建新的HtML(1. 结构和原来一样 2. 会自动引入打包的资源)
    }),
  ],

  //自动打包编译运行  1 修改webpack配置对象  不是在loader中  2 修改packge.json中的scripts中的指令  3 运行npm run start
  //"webpack": "^4.44.2","webpack-cli": "^3.3.12",第一个版本
  devServer: {
    open: true, // 自动打开浏览器
    compress: true, // 启动gzip压缩
    port: 3000, // 端口号
    //问题:html文件无法自动更新了,需要增加一个入口..(现webpack4已无bug) entry: ['./src/js/app.js','./src/index.html']
    hot: true, // 开启热模替换功能 HMR
  },
  // 注意: 跟entry,output,mode,module,plugins是同一级
  devtool: "cheap-module-eval-source-map", // 开发环境下
  mode: "development",//开发模式
};
