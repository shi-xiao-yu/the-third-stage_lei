1   gulp是与webpack功能类似的前端项目构建工具 也是基于nodejs的自动任务运行器  
2   能自动化的完成文件的合并压缩 检查  监听文件的变化 浏览器自动刷新 测试等任务  
3   gulp更高效异步多任务   更易于使用 插件高质量



相关的插件

gulp-concat 合并js/css文件  gulp-cssmin 压缩css   gulp-uglify 压缩js文件	gulp-rename  文件重命名  gulp-livereload  实时自动贬义词刷新



重要的api

gulp.src(filepath/patharr)   指向指定路径的所有文件 返回文件流对象  用于读取文件

gulp.dest(dirPath/pathArr) 指向指定的所有文件夹  用于向文件夹中输出文件

gulp.task(name,[deps],fn) 定义一个任务

gulp.watch()监视文件的变化