webpack是一个模块打包器(bundler)

在webpack看来,前端的所有资源文件都会作为模块处理

他将根据模块的依赖关系进行静态分析,生成对应的静态资源





五个核心概念:

entry:入口起点,指示webpack应该使用哪个模块,来作为构建其内部依赖图的开始

output: 告诉webpack在哪里输出它所创建的bundles,以及如何命名这些文件,默认值为./dist

loader:去处理那些非js/json文件

plugins:插件可以执行的范围更大,执行loader执行不了的事

mode:模式,有生产模式production和开发模式development



理解loader:

webpack只能加载js/json模块,如果要加载其他类型的文件(模块)就需要使用对应的loader进行转换/加载

loader本身也是运行在node.js环境中的js模块

它本身也是一个函数,接收源文件作为参数,返回转换的结果

loader一般以xxx-loader的方式命名,xxx代表了这个loader要做的转换功能,比如json-loader



理解plugins:

插件可以完成一些loader不能完成的功能

插件的使用一般是在webpack的配置信息plugins选项中指定的



配置文件(默认)

webpack.config.js:是一个node模块,返回一个json格式的配置信息对象