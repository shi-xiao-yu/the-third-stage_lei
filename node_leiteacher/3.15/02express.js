const express = require('express')
const path = require('path')
const app = express();
app.use(express.urlencoded({extended:true}));
app.get('/getData/:name',(req,res)=>{
    const name = req.params.name;
    const pathStr = path.resolve(__dirname,'public',name)
    console.log(req.params);
    res.sendFile(pathStr)
})
app.post('/postData',(req,res)=>{
    console.log(req.body);
    res.send('成功')
})
app.listen(3000,(err)=>{
    if(err) return console.log('失败');
    console.log('成功');
})