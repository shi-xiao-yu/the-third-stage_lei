const express = require('express')
// const bodyParser = require('body-parser')
const app =express()
app.use(express.urlencoded({extended:true}));
app.all('*',(req,res,next)=>{
    console.log('拦截器');
    next()
})
app.get('/:id?',(req,res)=>{
    console.log(req.params);
    console.log(req.url);
    res.send('响应成功')
})
app.post('/',(req,res)=>{
    console.log(req.body);
    res.send('成功')
})
app.listen(3000,(err)=>{
    if(err) return console.log('失败',err);
    console.log('成功');
})