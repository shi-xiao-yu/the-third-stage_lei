const mongoose = require('mongoose')
module.exports = new mongoose.Schema(
    {
        id:{
            type:String,
            required:true,
            unique:true,
    
        },
        name:{
            type:String,
            required:true,
    
        },
        age:String,
        gender:{
            type:String,
            default:'女'
        } 
    },
    {
        collection:'student'
    }
)