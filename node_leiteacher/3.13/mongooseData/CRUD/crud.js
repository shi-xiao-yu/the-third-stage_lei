(async function () {
  const stuModel = require("../model/stuModel");
  const db = require("../db/db");
  try {
    await db;
    console.log("服务器启动成功");
    stuModel.create({
      id: Date.now(),
      name: "英语",
      age: 23,
      gender: "男",
    });
  } catch (err) {
    console.log("服务器启动失败", err);
  }
})();
