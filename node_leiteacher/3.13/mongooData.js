const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/playground',{
    useNewUrlParser: true ,
    useUnifiedTopology: true,
})
.then(()=>{console.log('成功');})
.catch(()=>{console.log('失败');})
const studentSchema = new mongoose.Schema({
    id:{
        type:String,
        required:true,
        unique:true,

    },
    name:{
        type:String,
        required:true,

    },
    age:String,
    gender:{
        type:String,
        default:'女'
    }
},{
    collection:'student'
})
const Student = mongoose.model('student',studentSchema)
const stuDocument = new Student({
    id:Date.now(),
    name:'海绵111',
    age:15,
    gender:'男'
})
stuDocument.create()