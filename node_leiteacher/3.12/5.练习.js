/* function add(...args) {
    console.log(args);
}
add(1,2,3,4,5)
function add1(a,b,...args) {
    console.log(a,b,args);
}
add1(100,1,2,3,4,5) */
/* let tfboys = ["德玛西亚之力", "德玛西亚之翼", "德玛西亚皇子"];
function fn() {
  console.log(arguments);
}
fn(...tfboys); */
/* let skillOne = {
  q: "致命打击",
};
let skillTwo = {
  w: "勇气",
};
let skillThree = {
  e: "审判",
};
let skillFour = {
  r: "德玛西亚正义",
};
let add = { ...skillOne, ...skillTwo, ...skillThree, ...skillFour };
console.log(add); */
//创建 Symbol
let s1 = Symbol();
console.log(s1, typeof s1);
//添加标识的 Symbol
let s2 = Symbol('尚硅谷');
let s2_2 = Symbol('尚硅谷');
console.log(s2 === s2_2);
//使用 Symbol for 定义
let s3 = Symbol.for('尚硅谷');
let s3_2 = Symbol.for('尚硅谷');
console.log(s3 === s3_2);
