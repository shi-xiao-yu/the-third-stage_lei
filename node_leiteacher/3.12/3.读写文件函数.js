const fs = require("fs");
function readWriteFile(readFile, writeFile) {
  return new Promise((resolve, reject) => {
    const rs = fs.createReadStream(readFile); // 创建可读流
    let ws = fs.createWriteStream(writeFile); // 创建可写流
    rs.pipe(ws);
    rs.on('close',()=>{
        console.log('读取关闭');
    })
    ws.on('close',()=>{
        console.log('写入关闭');
        resolve()
    })
  });
}

module.exports = {
    readWriteFile,
  }
