function myAjax(options) {
  //如果配置为空或者配置不是一个对象就return
  if (!options || typeof options !== "object") return;
  let {
    url,
    type = "get" ,
    data,
    success,
    error,
    beforeSend,
    complete,
  } = options;
  //如果地址为空return 
  if (!url) return;
  const xhr = new XMLHttpRequest();
  //data不为空和data数据转换为json字符串 
  data = data && json2Str(data);
  if (type.toLowerCase() === "get" && data) {
    url += "?" + data;
    data = null;
  }
  xhr.open(type, url);
  type.toLowerCase() === "post"  &&
    xhr.setRequestHeader("content-type", "application/x-www-form-urlencoded");
    const res = beforeSend && beforeSend()
    if(res === false) return
  xhr.send(data);
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      complete && complete()
      if (xhr.status === 200) {
        success && success(xhr.responseText)
      }else {
        error && error()
      }
    }
  };
}
function json2Str(data) {
  //data没有或者检测的类型不为object  return
  if (!data || typeof data !== "object") return;
  let arr = [];
  for (let key in data) {
    arr.push(key + "=" + data[key]);
  }
  return arr.join("&");
}
