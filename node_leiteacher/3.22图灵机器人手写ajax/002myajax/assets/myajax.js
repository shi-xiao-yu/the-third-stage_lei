function myAjax(options) {
  if (!options || typeof options !== "object") return;
  let {
    url,
    type = "get",
    data,
    success,
    error,
    beforeSend,
    complete,
  } = options;
  if (!url) return;
  const xhr = new XMLHttpRequest();
  data = data && json2data(data);
  if (type.toLowerCase() === "get" && data) {
    url += "?" + data;
    data = null;
  }
  xhr.open(type, url);
  type.toLowerCase() === "post" &&
    xhr.setRequestHeader("content-type", "application/x-www-form-urlencoded");
  const res = beforeSend && beforeSend();
  if (res === false) return;
  xhr.send(data);
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      complete && complete();
      if (xhr.status === 200) {
        success && success(xhr.responseText);
      } else {
        error && error();
      }
    }
  };
}
function json2data(data) {
  if (!data || typeof data !== "object") return;
  let arr = [];
  for (let key in data) {
    arr.push(key + "=" + data[key]);
  }
  return arr.join("&");
}
