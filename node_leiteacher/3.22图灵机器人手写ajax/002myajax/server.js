const express = require('express')
const cors = require('cors')
const app = express()
app.listen(3000,err=>{
    if(err) console.log('服务器连接失败');
    console.log('服务器连接成功');
})
app.use(cors())
app.use(express.urlencoded({extended:true}))
app.get('/test',(req,res)=>{
    res.send(req.query)
})
app.post('/test',(req,res)=>{
    res.send(req.body)
})