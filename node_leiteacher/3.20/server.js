const express = require("express");
const app = express();
app.use(express.static("public"));
app.use(express.urlencoded({ extended: true }));
app.use((req,res,next)=>{
    res.set('Access-Control-Allow-Origin','*')
    next()
})
app.post("/validatorUsername", (req, res) => {
  const { username } = req.body;
  const arr = ["钢铁侠", "蜘蛛侠", "蝙蝠侠", "猪猪侠", "大侠", "煎饼侠"];
  const result = arr.findIndex((item) => {
    return item === username;
  });
  if (result >= 0) {
    res.send({ code: 40000, data: {}, message: "用户被占用" });
  } else {
    res.send({ code: 20000, data: {}, message: "用户可用" });
  }
});
/* app.get('/jsonpTest',(req,res)=>{
    const {callback} = req.query
    res.send(callback+'({name: "zs", age: 18})')
}) */
/* app.get("/jqueryJsonp", (req, res) => {
    const {callback} = req.query
    res.send(callback + '({name: "zs", age: 18})');
}); */
app.get("/jqueryJsonp", (req, res) => {
  res.send('{"name": "zs", "age": 18}');
});
app.listen(3000);
console.log("成功");
