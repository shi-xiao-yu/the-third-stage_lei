const express = require('express')
const model = require('../mongooseData/model/model')
const router = express.Router()
router.post("/register", async(req, res) => {
    const { email, username, psd } = req.body;
    try{
      await model.create({
          email,
          username,
          psd
      })
      res.redirect('http://localhost:3000/login.html')
    }
    catch(err){
        res.send('注册失败')
    }
  });
router.post("/login", async(req, res) => {
    const {email,psd} = req.body
    const user = await model.findOne({email,psd})
    const userName = user.username
    if(user) {
        res.redirect(`http://localhost:3000/?username=${userName}`)
    }else {
        res.send('用户名密码不正确')
    }
  });
  module.exports = router