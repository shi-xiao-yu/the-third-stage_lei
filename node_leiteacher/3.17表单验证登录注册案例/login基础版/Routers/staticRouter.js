const express = require('express')
const path =require('path')
const router = express.Router()
router.get('/register',(req,res)=>{
    const url = path.resolve(__dirname,'../public/register.html')
    res.sendFile(url)
})
router.get('/login',(req,res)=>{
    const url = path.resolve(__dirname,'../public/login.html')
    res.sendFile(url)
})
module.exports = router