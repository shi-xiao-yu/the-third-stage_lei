

(async function () {
  const express = require("express");
  const loginRouter = require('./Routers/userRouter')
  const staticRouter = require('./Routers/staticRouter')
  const db = require("./mongooseData/db/db");
  try {
    await db;
    console.log('数据库连接成功');
    const app = express();
    app.use(express.urlencoded({ extended: true }));
    app.use(express.static('public'))
    app.use(staticRouter)
    app.use(loginRouter)
    app.listen(3000, (err) => {
      if (err) console.log("失败");
      console.log("成功");
    });
  }
  catch(err) {
    console.log('数据库连接失败');
  }
})();
