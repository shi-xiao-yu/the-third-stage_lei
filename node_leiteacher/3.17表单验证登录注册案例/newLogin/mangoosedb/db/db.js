const mongoose = require('mongoose')
mongoose.set('useCreateIndex', true)
module.exports = mongoose.connect('mongodb://localhost/playground',{
    useNewUrlParser: true,
    useUnifiedTopology: true
})