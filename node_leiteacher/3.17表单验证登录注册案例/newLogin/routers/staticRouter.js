const express = require("express");
const path = require("path");
const router = express.Router();
router.get("/register", (req, res) => {
  const staticPath = path.resolve(__dirname, "../public/register.html");
  res.sendFile(staticPath);
});
router.get("/login", (req, res) => {
  const staticPath = path.resolve(__dirname, "../public/login.html");
  res.sendFile(staticPath);
});
router.get("/user", (req, res) => {
  const staticPath = path.resolve(__dirname, "../public/index.html");
  const { uid } = req.session;
  if (uid) {
    res.sendFile(staticPath);
  } else {
    res.redirect("http://localhost:5000/login");
  }
});
module.exports = router;
