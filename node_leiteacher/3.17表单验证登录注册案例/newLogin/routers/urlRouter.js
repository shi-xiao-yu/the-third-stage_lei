const express = require("express");
const userModel = require("../mangoosedb/model/model");
const router = express.Router();
router.post("/register", async (req, res) => {
  const { email, username, psd } = req.body;
  try {
    await userModel.create({
      email,
      username,
      psd,
    });
    res.redirect("http://localhost:5000/login");
  } catch (err) {
    res.send("注册失败");
  }
});
router.post("/login", async (req, res) => {
  const { email, psd } = req.body;
  const userLogin = await userModel.findOne({
    email,
    psd,
  });
  if(userLogin) {
    req.session.uid = userLogin._id
    res.redirect(`http://localhost:5000/user?username=${userLogin.username}`)
  }else {
    res.send('邮箱或密码不正确')
  }
});

module.exports = router;
