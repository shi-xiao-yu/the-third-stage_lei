const express = require("express");
const model = require("../mongoosedb/model/model");
const userModel = require("../mongoosedb/model/model");
const router = express.Router();
router.post("/register", async (req, res) => {
  const { email, username, psd } = req.body;
  await userModel.create({
    email,
    username,
    psd,
  });
  res.redirect("http://localhost:3000/login");
});
router.post("/login", async (req, res) => {
  const { username, psd } = req.body;
  // console.log(req.body);
  const user = await model.findOne({
    username,
    psd,
  });
  if (user) {
    req.session.userid = user._id
    res.redirect(`http://localhost:3000/?username=${user.username}`);
  } else {
    res.send("用户名密码错误");
  }
});
module.exports = router;
