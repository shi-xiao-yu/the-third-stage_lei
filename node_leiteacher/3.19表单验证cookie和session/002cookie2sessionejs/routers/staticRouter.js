const express = require("express");
const path = require('path');
const model = require("../mongoosedb/model/model");
const userModel = require('../mongoosedb/model/model')
const router = express.Router();
router.get("/register", (req, res) => {
  const registerPath = path.resolve(__dirname, "../public/register.html");
  res.sendFile(registerPath);
});
router.get("/login", (req, res) => {
  const loginPath = path.resolve(__dirname, "../public/login.html");
  res.sendFile(loginPath);
});
router.get("/", async(req, res) => {
  // const indexPath = path.resolve(__dirname, "../public/index.html");
  if(req.session.userid) {
    const user = await model.findOne({_id:req.session.userid})
  // res.sendFile(indexPath);
  res.render('index',{user})

  }else {
    res.redirect('http://localhost:3000/login')
  }
});
module.exports = router
