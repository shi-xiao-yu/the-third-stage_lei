(async () => {
  const express = require("express");
  const app = express();
  const cookieParser = require('cookie-parser')
  const session = require('express-session')
  const staticRouter = require("./routers/staticRouter");
  const uiRouter = require("./routers/uiRouter");
  const db = require("./mongoosedb/db/db");
  try {
    await db;
    console.log('数据库连接成功');
    app.use(express.urlencoded({ extended: true }));
    app.set('view engine','ejs')
    app.set('views','./templates')
    app.use(cookieParser())
    let MongoStore = require('connect-mongo')(session)
    app.use(
      session({
        name: 'id22', //设置cookie的name，默认值是：connect.sid
        secret: 'atguigu', //参与加密的字符串（又称签名）
        saveUninitialized: false, //是否为每次请求都设置一个cookie用来存储session的id
        resave: true, //是否在每次请求时重新保存session
        store: new MongoStore({
          url: 'mongodb://localhost:27017/test-app',
          touchAfter: 24 * 3600, // 24小时之内只修改一次
        }),
        cookie: {
          httpOnly: true, // 开启后前端无法通过 JS 操作
          maxAge: 1000 * 30, // 这一条 是控制 sessionID 的过期时间的！！！
        },
      })
    )
    app.use(staticRouter);
    app.use(uiRouter);
    app.listen(3000, (err) => {
      if (err) console.log("服务器启动失败", err);
      console.log("服务器启动成功");
    });
  } catch(err) {
    console.log('数据库连接失败',err);
  }
})();
